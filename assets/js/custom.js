function handleAjaxRequest(e, form, call_url, response_div) {
    e.preventDefault();

    var datastring = $(form).serialize();
    $.ajax({
        type: "POST",
        url: call_url,
        data: datastring,
        dataType: "text",
        success: function(data) {
            $(response_div).empty();
            $(response_div).append(data);
        },
        error: function(error) {
            alert('Error attempting to do Ajax request.');
            console.log(error);
        }
    });
}